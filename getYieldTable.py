#!/usr/bin/env python

import pandas as pd
import numpy as np
from math import pi as PI
import math
from argparse import ArgumentParser
import sys
import os
from collections import OrderedDict
from tools.LateXTable import MakeTEXTable

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for NTupleToNumpy for BDT")
	args.add_argument('-v', '--var', action='store', required=True, help='Variable to cut on')
	args.add_argument('-c', '--cut', action='store', required=True, help='Cut')
	return args.parse_args()
	
def GetDataDic(path):
	
	HyGr   = pd.read_hdf(path+'HyGr.h5', 'df')
	HyGr1  = pd.read_hdf(path+'HyGr1.h5', 'df')
	HyGr10 = pd.read_hdf(path+'HyGr10.h5', 'df')
	HyGr20 = pd.read_hdf(path+'HyGr20.h5', 'df')	
	HyGr30 = pd.read_hdf(path+'HyGr30.h5', 'df')
	HyGr40 = pd.read_hdf(path+'HyGr40.h5', 'df')
	
	HZy     = pd.read_hdf(path+'HZy.h5', 'df')
	Top     = pd.read_hdf(path+'Top.h5', 'df')
	VV      = pd.read_hdf(path+'VV.h5', 'df')
	VVV     = pd.read_hdf(path+'VVV.h5', 'df')
	Wg_jets = pd.read_hdf(path+'Wg_jets.h5', 'df')
	Zg_jets = pd.read_hdf(path+'Zg_jets.h5', 'df')
	Z_jets  = pd.read_hdf(path+'Z_jets.h5', 'df')
	
	ZZZ_sum_bkg = HZy.append(Top, ignore_index=True)
	ZZZ_sum_bkg = ZZZ_sum_bkg.append(VV, ignore_index=True)
	ZZZ_sum_bkg = ZZZ_sum_bkg.append(VVV, ignore_index=True)
	ZZZ_sum_bkg = ZZZ_sum_bkg.append(Wg_jets, ignore_index=True)
	ZZZ_sum_bkg = ZZZ_sum_bkg.append(Zg_jets, ignore_index=True)
	ZZZ_sum_bkg = ZZZ_sum_bkg.append(Z_jets, ignore_index=True)
	
	dic =OrderedDict( {'HyGr': HyGr, 'HyGr1': HyGr1, 'HyGr10': HyGr10, 'HyGr20': HyGr20, 'HyGr30': HyGr30, 'HyGr40': HyGr40, 'SingleH': HZy, 'Top': Top, 'VV': VV, 'VVV': VVV, 'Wg_jets': Wg_jets, 'Zg_jets': Zg_jets, 'Z_jets': Z_jets, 'Zg_sum_bkg': ZZZ_sum_bkg})
	
	return dic

def getYield_Baseline(df):
	
	df = df[df.isBaseline == 1]
	
	return ComputeYield(df)
	
def getYield_SR(df, chan):

	df = df[df.isSR == 1]
	#df = df[df.isLetPt == 1]
	
	df = passChannel(df, chan)	
	
	return ComputeYield(df)	
	
def getYield_SR_Lep(df, chan):
	
	df = df[df.isSR == 1]
	df = df[df.isLetPt == 1]
	
	df = passChannel(df, chan)	
	
	return ComputeYield(df)
	
def getYield_BDT(df, chan, bdt, cut):

	df = df[df.isSR == 1]
	df = df[df[bdt] > cut]
	
	df = passChannel(df, chan)	
	
	return ComputeYield(df)	
	
def getYield_AHOI_BDT(df, chan, bdt, cut):

	df = df[df.isSR == 1]
	df = df[df.isLetPt == 1]
	df = df[df.isAHOI == 1]
	df = df[df[bdt] > cut]
	
	df = passChannel(df, chan)			
	
	return ComputeYield(df)		

def getYield_AHOI(df, chan):

	df = df[df.isSR == 1]
	df = df[df.isLetPt == 1]
	df = df[df.isAHOI == 1]
	df = passChannel(df, chan)		
		
	return ComputeYield(df)		
	
def ComputeYield(df):	
	
	y = df.weight.sum()
	
	y_err = math.sqrt((df.weight*df.weight).sum())
	
	return [y, y_err]
	
def passChannel(df, chan): 

	if chan == 0: 
		df = df[df.isEl  == 1]
	if chan == 1:
		df = df[df.isMu  == 1]
	if chan == 2:
		eL = df.isEl == 1
		mU = df.isMu == 1
		df = df[eL | mU]
		
	return df
			
def getChannel(chan):
	if chan == 0:
		return 'ee'
	if chan == 1:
		return 'mumu'
	if chan == 2:
		return 'll'	
		
def getProccessName(process):
	
	dic = {'HyGr': 'ZH($\gamma\gamma_{d}$)', 'HyGr1': 'ZH($\gamma\gamma_{d}$ 1)', 'HyGr10': 'ZH($\gamma\gamma_{d}$ 10)', 'HyGr20': 'ZH($\gamma\gamma_{d}$ 20)', 'HyGr30': 'ZH($\gamma\gamma_{d}$ 30)', 'HyGr40': 'ZH($\gamma\gamma_{d}$ 40)', 'SingleH': 'Higgs', 'Top': 'Top', 'VV': 'VV', 'VVV': 'VVV', 'Wg_jets': 'W$\gamma$+jets', 'Zg_jets': 'Z$\gamma$+jets', 'Z_jets': 'Z+jets', 'Zg_sum_bkg': 'Sum of bkg'}
	
	return dic[process]	
					
def main():
	args=getArgs()
	path = '/afs/cern.ch/user/m/mobelfki/PostDoc/ZHDarkPhoton/BDTComparison/DataFrames/'
	data = GetDataDic(path)
	
	for chan in range(3):
		dic = OrderedDict()
		print("Channel", chan)
		for name, df in sorted(data.items()):
			#print(name)
			y_b       = getYield_Baseline(df)
			y_sr      = getYield_SR(df, chan)
			y_sr_lep  = getYield_SR_Lep(df, chan)
			y_ahoi    = getYield_AHOI(df, chan)
			y_bdt     = getYield_BDT(df, chan, str(args.var), float(args.cut))
			y_ahoi_bdt= getYield_AHOI_BDT(df, chan, str(args.var), float(args.cut))
			
			tmp_dic = {getProccessName(name): {'Baseline': y_b, 'SR': y_sr, 'SR+Lep': y_sr_lep, 'SR+AHOI': y_ahoi, 'SR+BDT': y_bdt, 'SR+AHOI+BDT': y_ahoi_bdt}}
			
			dic.update(OrderedDict(tmp_dic))
			
		MakeTEXTable(dic, 'YieldTable_Channel_'+getChannel(chan)+'_Var_'+str(args.var)+'_Cut_'+str(args.cut)+'.tex')
		os.system('pdflatex '+'YieldTable_Channel_'+getChannel(chan)+'_Var_'+str(args.var)+'_Cut_'+str(args.cut)+'.tex > log.log')	
	os.system('rm *.log *.aux *.out')
if __name__ == '__main__':
	main()	
