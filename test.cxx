{

	TFile* mc16a = new TFile("/eos/user/h/heljarra/V17Sys/SR/OutputMC16a_V17_SR.root", "READ");
	TFile* mc16d = new TFile("/eos/user/h/heljarra/V17Sys/SR/OutputMC16d_V17_SR.root", "READ");
	TFile* mc16e = new TFile("/eos/user/h/heljarra/V17Sys/SR/OutputMC16e_V17_SR.root", "READ");	
	
	
	TH1D* h_uu_mc16a = (TH1D*) mc16a->Get("pass_sr_all_uu_Nominal/plotEvent_EltoPhFakes/met_tst_et");
	TH1D* h_ee_mc16a = (TH1D*) mc16a->Get("pass_sr_all_ee_Nominal/plotEvent_EltoPhFakes/met_tst_et");
	
	TH1D* h_uu_mc16d = (TH1D*) mc16d->Get("pass_sr_all_uu_Nominal/plotEvent_EltoPhFakes/met_tst_et");
	TH1D* h_ee_mc16d = (TH1D*) mc16d->Get("pass_sr_all_ee_Nominal/plotEvent_EltoPhFakes/met_tst_et");
	
	TH1D* h_uu_mc16e = (TH1D*) mc16e->Get("pass_sr_all_uu_Nominal/plotEvent_EltoPhFakes/met_tst_et");
	TH1D* h_ee_mc16e = (TH1D*) mc16e->Get("pass_sr_all_ee_Nominal/plotEvent_EltoPhFakes/met_tst_et");	
	
	cout<< h_uu_mc16a->Integral() <<endl;
	cout<< h_uu_mc16a->Integral() + h_ee_mc16a->Integral() + h_uu_mc16d->Integral() + h_ee_mc16d->Integral() + h_uu_mc16e->Integral() + h_ee_mc16e->Integral() <<endl;	

}
