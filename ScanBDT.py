#!/usr/bin/env python

import pandas as pd
import numpy as np
from math import pi as PI
import math
from math import log, sqrt
import shutil
from argparse import ArgumentParser
import sys
import os
from collections import OrderedDict
import matplotlib.pyplot as plt
from prettytable import PrettyTable

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for NTupleToNumpy for BDT")
	args.add_argument('-v', '--vars', action='store', default={'BDT_weight_noAHOI', 'BDT_weight_wAHOI'}, help='Variable to cut on')
	args.add_argument('-c', '--cuts', action='store', default={'BDT_weight_noAHOI': 0.99 , 'BDT_weight_wAHOI': 0.96}, help= 'Cuts value to apply when computing the yields')
	args.add_argument('-f', '--doFix', action='store', default=True, help='Allow for fixed background')
	args.add_argument('-b', '--Bkg', action='store', default=0.8, help='The background')
	args.add_argument('-B', '--doBootstrap', action='store', default=False, help='do bootstrap method')
	args.add_argument('-N', '--Nstories', action='store', default=1000, help='N stories for bootstrap')
	
	return args.parse_args()

def plot_roc_curve(sig_eff, bkg_rej, z_max, z_unc, index, sig, name, gpath):

	plt.figure()
	plt.plot(sig_eff, bkg_rej, color='darkorange', lw=2, label=sig)
	plt.plot(sig_eff[index], bkg_rej[index], 'ob')
	plt.xlim([0., 1.0])
	plt.ylim([0., 1.0])
	plt.xlabel('sig. eff.')
	plt.ylabel('1 - bkg. rej.')
	plt.title('ROC for ' + sig)
	plt.legend(['ROC', '(eff: %0.2f, rej: %0.2f, AMS: %0.2f +/- %0.2f)'%(sig_eff[index], bkg_rej[index], z_max, z_unc)], loc="lower left")
	plt.savefig(gpath+'/'+name+'.pdf')
	plt.close()
		
def plot_multi_roc_curve(Ana, name, gpath):
	
	plt.figure()
	leg = []
	
	for sig, item in Ana.items():
		sig_eff, bkg_rej, z_max, index, z_unc = item[0], item[1], item[3], item[4], item[-1]
		plt.plot(sig_eff, bkg_rej, lw=2, label=sig)
		leg.append(sig+' (eff: %0.2f, rej: %0.2f, AMS: %0.2f +/- %0.2f)'%(sig_eff[index], bkg_rej[index], z_max, z_unc[index]))
		
	plt.xlim([0., 1.0])
	plt.ylim([0., 1.0])
	plt.xlabel('sig. eff.')
	plt.ylabel('1 - bkg. rej.')
	plt.title('ROC')
	plt.legend(leg, loc="lower left")
	plt.savefig(gpath+'/ROC_all_'+name+'.pdf')
	plt.close()	

def plot_ams(var, z, z_max, z_unc, index, xlabel, sig, name, gpath):

	plt.figure()
	plt.plot(var, z, color='darkorange', lw=2, label=sig)
	plt.plot(var[index], z_max, 'ob')
	plt.xlim([0., 1.0])
	plt.xlabel(xlabel)
	plt.ylabel('AMS')
	plt.legend(['AMS', '(cut: %0.2f, AMS: %0.2f +/- %0.2f)'%(var[index], z_max, z_unc)], loc="lower left")
	plt.title('AMS for ' + sig)
	plt.savefig(gpath+'/'+name+'.pdf')
	plt.close()
	
	
def plot_ams_bootstrap(eff, rej, z, xlabel, sig, name, gpath):
	
		plt.figure()
		plt.hist(z, bins = 50, histtype='step') 	
		plt.xlabel('AMS '+xlabel)
		plt.ylabel('Entries')
		plt.legend(['AMS', '(eff: %0.2f, rej: %0.2f, AMS: %0.2f +/- %0.2f)'%(eff, rej, np.mean(z), np.std(z))], loc="lower left")
		plt.title('Bootstraped AMS for ' + sig)
		plt.savefig(gpath+'/'+name+'.pdf')
		plt.close()
		
	
def plot_multi_ams(Ana, var, name, gpath):

	plt.figure()
	leg = []
	for sig, item in Ana.items():
		s_eff, z, z_max, index, z_unc = item[0], item[2], item[3], item[4], item[-1]
		plt.plot(s_eff, z, lw=2, label=sig)
		leg.append(sig+' (eff: %0.2f, AMS: %0.2f +/- %0.2f)'%(s_eff[index], z_max, z_unc[index]))
		
	plt.xlim([0., 1.0])
	plt.xlabel('sig. eff.')
	plt.ylabel('AMS')
	plt.legend(leg, loc="lower left")
	plt.title('AMS')
	plt.savefig(gpath+'/Z_vs_Sig_all_'+name+'.pdf')
	plt.close()	
	
	plt.figure()
	leg = []
	for sig, item in Ana.items():
		b_rej, z, z_max, index, z_unc = item[1], item[2], item[3], item[4], item[-1]
		plt.plot(s_eff, z, lw=2, label=sig)
		leg.append(sig+' (rej: %0.2f, AMS: %0.2f +/- %0.2f)'%(b_rej[index], z_max, z_unc[index]))
		
	plt.xlim([0., 1.0])
	plt.xlabel('1 - bkg. rej.')
	plt.ylabel('AMS')
	plt.legend(leg, loc="lower left")
	plt.title('AMS')
	plt.savefig(gpath+'/Z_vs_Bkg_all_'+name+'.pdf')
	plt.close()	
	
	
	plt.figure()
	leg = []
	for sig, item in Ana.items():
		bdt, z, z_max, index, z_unc = item[5], item[2], item[3], item[4], item[-1]
		if z[index] != z_max:
			print(' You are doing something wrong, will exit()')
			exit()
		plt.plot(bdt, z, lw=2, label=sig)
		leg.append(sig+' (cut: %0.2f, AMS: %0.2f +/- %0.2f)'%(bdt[index], z_max, z_unc[index]))
		
	plt.xlim([0., 1.0])
	plt.xlabel(getLabel(var))
	plt.ylabel('AMS')
	plt.legend(leg, loc="lower left")
	plt.title('AMS')
	plt.savefig(gpath+'/Z_vs_BDT_all_'+name+'.pdf')
	plt.close()	

def GetDataDic(path, chan):
	
	HyGr   = pd.read_hdf(path+'HyGr.h5', 'df')
	HyGr1  = pd.read_hdf(path+'HyGr1.h5', 'df')
	HyGr10 = pd.read_hdf(path+'HyGr10.h5', 'df')
	HyGr20 = pd.read_hdf(path+'HyGr20.h5', 'df')	
	HyGr30 = pd.read_hdf(path+'HyGr30.h5', 'df')
	HyGr40 = pd.read_hdf(path+'HyGr40.h5', 'df')
	
	HZy     = pd.read_hdf(path+'HZy.h5', 'df')
	Top     = pd.read_hdf(path+'Top.h5', 'df')
	VV      = pd.read_hdf(path+'VV.h5', 'df')
	VVV     = pd.read_hdf(path+'VVV.h5', 'df')
	Wg_jets = pd.read_hdf(path+'Wg_jets.h5', 'df')
	Zg_jets = pd.read_hdf(path+'Zg_jets.h5', 'df')
	Z_jets  = pd.read_hdf(path+'Z_jets.h5', 'df')
	
	dic = {'HyGr': passSR(HyGr, chan), 'HyGr1': passSR(HyGr, chan), 'HyGr10': passSR(HyGr10, chan), 'HyGr20': passSR(HyGr20, chan), 'HyGr30': passSR(HyGr30, chan), 'HyGr40': passSR(HyGr40, chan), 'SingleH': passSR(HZy, chan), 'Top': passSR(Top, chan), 'VV': passSR(VV, chan), 'VVV': passSR(VVV, chan), 'Wg_jets': passSR(Wg_jets, chan), 'Zg_jets': passSR(Zg_jets, chan), 'Z_jets': passSR(Z_jets, chan)}
	
	return dic
	

def passBaseline(df):

	df = df[df.isBaseline == 1]
	return df

def passSR(df, chan):
	
	df = df[df.isBaseline == 1]
	df = df[df.isSR == 1]
	df = df[df.isAHOI == 1]
	df = passChannel(df, chan)
	
	return df
	
def passChannel(df, chan): 

	if chan == 0: 
		df = df[df.isEl  == 1]
	if chan == 1:
		df = df[df.isMu  == 1]
	if chan == 2:
		eL = df.isEl == 1
		mU = df.isMu == 1
		df = df[eL | mU]
	return df
	
def passBDT(df, var, cut):

	df = df[df[var] > cut]
	return df
	
def ComputeYield(df):	
	
	y = df.weight.sum()
	
	y_err = math.sqrt((df.weight*df.weight).sum())
	
	if getArgs().doBootstrap:
	
		y_boostrap = {}
		
		for i in range(int(getArgs().Nstories)):
			poisson_value = np.random.poisson(1,len(df.weight))
			weight = df.weight*poisson_value
			
			y_i = np.sum(weight)
			
			y_err_i = math.sqrt(np.sum(weight*weight))
			
			y_boostrap.update({i: [y_i, y_err_i]})
	
		return [y, y_err, y_boostrap]
	return [y, y_err]
	
def ComputeYield_in_mT(df):

	df = df[df.mT > 65]
	df = df[df.mT < 145]
	
	return ComputeYield(df)	
	
def ComputeZ(s, b, s_unc, b_unc):

  	if b<=0. or s<=0.:
  		 return [0, 0]
  	
  	l     = math.log(1+s/b);
  	Z0    = math.sqrt(2*((s+b)*l-s))
 	dZ0ds = l/Z0
 	dZ0db = (l-s/b)/Z0
  	Z0_unc = math.sqrt(dZ0ds*dZ0ds * s_unc*s_unc + dZ0db*dZ0db * b_unc*b_unc)
  	return [Z0, Z0_unc]
  	
def getLabel(s):

	if s == 'BDT_weight_noAHOI':
		return 'BDT noAHOI'
	if s == 'BDT_weight_wAHOI':
		return 'BDT wAHOI'
		
	if s == 'both':
		return 'BDT'
		
def ComputeZ_bootstrap(sig_y, bkg_y):

	Z = []
	
	for i in range(int(getArgs().Nstories)):
		
		b = 0
		b_err = 0
		
		for bkg, item in bkg_y.items():
		
			if bkg == 'bkg': 
				print('!!!!!!!!!!!!!!')
		
			b += item[2][i][0]
			b_err += item[2][i][0]*item[2][i][0]
		
		b_err = math.sqrt(b_err)
		
		s = sig_y[2][i][0]
		s_err = sig_y[2][i][1]
		
		z = ComputeZ(s, b, s_err, b_err)
		
		Z.append(z[0]) # no need for error

	return Z
		
def doScan(data, sigs, bkgs, var):
	Cuts = {}
	
	for cut in np.linspace(0., 1., num=100, endpoint=False):
	
		b = 0
		b_unc = 0
		
		s = 0 
		s_unc = 0
		
		bkg_y = {}
		bkg_y_b = {}
		sig_y = {}
		AMS = {}
		
		for bkg in bkgs: 
		
			df  = passBDT(data[bkg], var, cut)
			
			y   = ComputeYield(df)
			
			b += y[0]
			b_unc += y[1]*y[1]
			
			bkg_y.update({bkg: y})
			bkg_y_b.update({bkg: y}) # for bootstrap only
				
		bkg_y.update({'bkg': [b, math.sqrt(b_unc)]})

		for sig in sigs: 
		
			df  = passBDT(data[sig], var, cut)
			
			y   = ComputeYield(df)
			
			#y_in_mT = ComputeYield_in_mT(df)
			
			sig_y.update({sig: y})
			
			z = ComputeZ(y[0], bkg_y['bkg'][0], y[1], bkg_y['bkg'][1])
			
			z_b = {}
			
			if getArgs().doBootstrap:
			
				z_b = ComputeZ_bootstrap(y, bkg_y_b)
			
			AMS.update({sig: [z, z_b]})
			
		Cuts.update({cut: [cut, AMS, sig_y, bkg_y]})
	
	return Cuts

def doAnalysis(Cuts, sigs, var, gpath):

	Ana = {}
		
	for sig in sigs:
	
		s0 = Cuts[0][2][sig][0]
		b0 = Cuts[0][3]['bkg'][0]
		
		b     = []
		s     = []
		b_unc = []
		s_unc = []
		z     = []
		z_unc = []
		bdt   = []
		b_rej = []
		s_eff = []
		
		z_b = []
		
		for cut, item in sorted(Cuts.items()):
	
			z.append( item[1][sig][0][0])
			z_unc.append( item[1][sig][0][1])
			
			s.append( item[2][sig][0])
			s_unc.append( item[2][sig][1])
			
			b.append( item[3]['bkg'][0])
			b_unc.append( item[3]['bkg'][1])
			
			b_rej.append(1 - item[3]['bkg'][0]/b0)
			s_eff.append(item[2][sig][0]/s0)
			
			z_b.append(item[1][sig][1])
			
			bdt.append(item[0])
		
		z_max = np.max(z)
		index = np.argmax(z)
		z_max_unc = z_unc[index]
		
		plot_roc_curve(s_eff, b_rej, z_max, z_max_unc, index, sig, 'ROC_'+sig+'_'+var, gpath)
		plot_ams(bdt, z, z_max, z_max_unc, index, getLabel(var), sig, 'Z_vs_BDT_'+sig+'_'+var, gpath) 
		plot_ams(s_eff, z, z_max, z_max_unc, index, 'sig. eff.', sig, 'Z_vs_Sig_'+sig+'_'+var, gpath) 
		plot_ams(b_rej, z, z_max, z_max_unc, index, '1 - bkg. rej.', sig, 'Z_vs_Bkg_'+sig+'_'+var, gpath) 
		
		#plot_ams_bootstrap(bdt, z_b, getLabel(var), sig, 'Z_Bootstrap_'+sig+'_'+var, gpath)
		Ana.update({sig: [s_eff, b_rej, z, z_max, index, bdt, sig, s0, b0, s, b, z_b, z_unc]}) # always Z_unc at the end
	
	return Ana
	
def multiple_plots(Ana, var, gpath, sig):

	name = var
	if var == 'both':
		name = name+'_'+sig
		print('Comparison for '+sig)
		
	plot_multi_roc_curve(Ana, name, gpath) 
	plot_multi_ams(Ana, var, name, gpath)
		
def doComparison(Obs, varlist, sig, table, gpath):

	Ana = {}
	
	for var in varlist: 
		Ana.update({var: Obs[var][1][sig]})
	
	multiple_plots(Ana, 'both', gpath, sig)
	
	fixed_rejection(Ana, table, sig, gpath)
	

def getIndex(lst, epsilon):

	tmp = 1
	ind = -9
	for l in lst: 
		dif = abs(l-epsilon)
		if dif < tmp: 
			tmp = dif
			ind = lst.index(l)	
	return ind
		
def fixed_rejection(Ana, table, sig, gpath):

	if not getArgs().doFix:
		return
	
	epsilon = float(getArgs().Bkg)	
	print(' Fixed background rejection to %.2f '%(epsilon))
	
	#	Ana.update({sig: [s_eff, b_rej, z, z_max, index, bdt, sig, s0, b0, s, b, z_unc]})
	
	Zs = []
			
	for var, item in Ana.items():
		
		s_eff, b_rej, z, bdt, z_unc, z_b = item[0], item[1], item[2], item[5], item[-1], item[-2]
		index = getIndex(b_rej, epsilon)
		f_s_eff = s_eff[index]
		f_b_rej = b_rej[index]
		f_z     = z[index]
		f_bdt   = bdt[index]
		f_z_unc = z_unc[index]
		f_z_b   = z_b[index]
		
		if getArgs().doBootstrap:
			Zs.append(f_z_b)
			plot_ams_bootstrap(f_s_eff, f_b_rej, f_z_b, var, sig, "Z_bootstrap_"+str(sig)+"_"+var, gpath)
		else: 
			Zs.append(f_z)	
		
		table.add_row( [var, '%.2f'%(f_s_eff), '%.2f'%(f_b_rej), '%.2f'%(f_bdt), '%.2f'%(f_z), '%.2f'%(f_z_unc)])
		
		# generate yield table
		os.system('./getYieldTable.py -v %s -c %.2f'%(var, f_bdt));		
	
	Z1 = Zs[0]
	Z2 = Zs[1]
	if getArgs().doBootstrap:
		Z1 = np.array(Zs[0])
		Z2 = np.array(Zs[1])
	
		
	Z_def = (Z1 - Z2)/Z1
	
	Z_def_mean = Z_def
	Z_def_std  = 0
	
	if getArgs().doBootstrap:
	
		Z_def_mean = np.mean(Z_def)
		Z_def_std  = np.std(Z_def)
		
		plot_ams_bootstrap(0, epsilon, Z_def, "diff.", sig, "Z_bootstrap_"+str(sig)+"_diff", gpath)
	
	table.add_row( ['Relative diff.', '--', '--', '--', '%.2f'%(Z_def_mean), '%.2f'%(Z_def_std)])
		
	print(table)	
def main():

	args=getArgs()
	path = '/afs/cern.ch/user/m/mobelfki/PostDoc/ZHDarkPhoton/BDTComparison/DataFrames/'
	
	
	data = GetDataDic(path, 2)
	
	sigs = {'HyGr', 'HyGr1', 'HyGr10', 'HyGr20', 'HyGr30', 'HyGr40'}
	bkgs = {'SingleH', 'Top', 'VV', 'VVV', 'Wg_jets', 'Zg_jets', 'Z_jets'}
	
	tag = ''
	
	if args.doFix: 
		tag = 'Fixed_Rej_'+str(args.Bkg)+'_'
	
	Obs = {}
	
	
	for var in args.vars:
		print var
		print('Default: Baseline+SR+AHOI is applied')
		if getArgs().doBootstrap:
			print('Bootstrapping is activated')
			
		gpath = tag+str(var)
		
		if os.path.exists(gpath):
			shutil.rmtree(gpath)
		
		try:	
    			os.makedirs(gpath)
		except OSError:
    			pass
    			
    		Cuts = doScan(data, sigs, bkgs, var)
    		
    		Ana  = doAnalysis(Cuts, sigs, var, gpath)
    		
    		multiple_plots(Ana, var, gpath, '')
    		
    		Obs.update({var: [Cuts, Ana]})
    		
    	if len(args.vars) <= 1:
    		exit()
    		
	gpath = tag+'Compared'
	
	print('Start comparison')
		
	if os.path.exists(gpath):
    		shutil.rmtree(gpath)
		
	try:	
    		os.makedirs(gpath)
	except OSError:
    		pass
    
    	for sig in sigs:
    		table = PrettyTable(['BDT', 'Sig. Eff.', 'Bkg. Rej.', 'BDT cut', 'Z', 'Z err.'])
    		
    		doComparison(Obs, args.vars, sig, table, gpath)
    	
    	print("Compute Yields")
    	for var in args.vars:
    		print('./getYieldTable.py -v %s -c %s'%(var, dict(args.cuts)[var]))
		os.system('./getYieldTable.py -v %s -c %s'%(var, dict(args.cuts)[var]))		
    	
					
if __name__ == '__main__':
	main()	
